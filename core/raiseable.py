class Base(object):
    def __init__(self):
        self._init()

    def _init(self):
        pass


class Raiseable(object):
    _raises = []

    @classmethod
    def raises(cls):
        r = set()
        r |= set(cls._raises)
        for base in cls.__bases__:
            if hasattr(base, '_raises'):
                r |= set(base._raises)

        return tuple(r)

if __name__ == '__main__':
    b = base3()
    err = base3.raises()
    try:
        raise IOError()
    except base3.raises() as e:
        print e
