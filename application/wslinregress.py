from core.itertools2 import column
from util.workstreams import Workstreams


if __name__ == '__main__':
    from easyconfig import Config

    c = Config()

    with c.parser() as p:
        p.add_option('workstream', alias='k', required=True, multiple=True, unspecified_default=True)
        p.add_option('index', alias='i', required=True, multiple=True)

    if len(workstream) == 1:
        w = c.workstreams().open(*workstream)
    else:
        w = c.workstreams().join(*workstream)

    cols = w.slice(*index).clean()

    from core.itertools2 import column
    from stats.multivar import regression

    print len(cols)

    #print regression(list(cols))
    from scipy.stats import linregress
    print linregress(list(cols))
