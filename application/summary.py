from plyny.table import DataTable


if __name__ == '__main__':
    from easyconfig import Config
    c = Config()

    with c.parser() as p:
        p.str('workstream').shorthand('k').multiple().unspecified_default().required()

    dt = c.workstreams().open(workstream)
    x = [i for i, value in enumerate(dt.types()) if value in (int, float)]
    t = DataTable(*(dt.column_names[i] for i in x))
    t.add(*dt.sum(*x))
    t.add(*dt.mean(*x))
    t.add(*dt.min(*x))
    t.add(*dt.max(*x))

    print t.table()
