from plyny.presentation.plot import Plot
from plyny.workstreams import Manager

import matplotlib
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True


if __name__ == '__main__':
    from easyconfig import Config

    c = Config()

    with c.parser() as p:
        p.add_flag('cdf')
        p.add_int('x-end', alias='X')
        p.add_flag('lines', alias='l')
        p.add_option('workstream', alias='k', required=True, multiple=True, unspecified_default=True)
        p.add_option('index', alias='i', required=True, multiple=True)
        p.add_option('out-file', alias='o', required=True)
        p.add_option('sort-index', alias='s')
        p.add_option('error-index')
        p.set_mutually_required(
            p.add_option('operation'),
            p.add_option('op-index')
            )
        p.add_flag('grid', alias='g')
        p.add_option('xlabel')
        p.add_option('ylabel')

        p.set_mutually_required(
            p.add_float('ystart'),
            p.add_float('yend')
        )

        p.set_mutually_required(
            p.add_float('xstart'),
            p.add_float('xend')
        )

    if len(workstream) == 1:
        w = c.workstreams().open(workstream[0])
    else:
        w = c.workstreams().join(*workstream)

    if cdf:
        p = w.cdf(index[0])
    else:
        if sort_index:
            w.sort_by(sort_index)

        p = w.plot(*index)
#        p = Plot(w.enumerate().narrow(*index))

        if xlabel:
            p.xlabel = xlabel

        if ylabel:
            p.ylabel = ylabel

        if grid:
            p.grid = True

        if error_index:
            p.errorbar(w.columns(error_index))

        if xstart is not None:
            p.xlim = (xstart, xend)

        if ystart is not None:
            p.ylim = (ystart, yend)

        if lines:
            p.set_style(0, '-')
        else:
            p.set_style(0, 'None')

        if error_index:
            p.errorbar(w.columns(error_index))

    p.save('%s' % out_file)
