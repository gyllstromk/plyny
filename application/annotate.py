from plyny.table import DataTable


if __name__ == '__main__':
    from easyconfig import Config
    c = Config()

    with c.parser() as p:
        p.add_option('workstream', alias='k', required=True)
        p.add_option('key', alias='y', required=True)
        p.add_option('value', alias='v', required=True)
        p.add_option('value-type', alias='t', required=True)

    dt = c.workstreams().open(workstream)
    g = globals()['__builtins__'].__dict__
    if value_type not in g:
        print '%s not valid type' % value_type
    else:
        dt.config().key = g[value_type](value)
