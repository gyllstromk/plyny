class Presentation(object):
    """ Used as mixin (I think) with methods for presenting results via tables
    and plots. """

    def plot(self, xindex, *yindexes, **named_yindexes):
        """ Create plot using matplotlib. """

        if not named_yindexes:
            yindexes = self._check_indexes()
            del yindexes[yindexes.index(xindex)]
            named_yindexes = dict((str(i), self.narrow(xindex, index)) for i, index in enumerate(yindexes))
        else:
            named_yindexes = dict((name, self.narrow(xindex, index)) for name, index in named_yindexes.iteritems())

        from plyny.presentation.plot import Plot

#        if use_range:
#            new_indexes = self._int_indexes_to_column_names(*self._indexes(*indexes))
#            slc = self.narrow(*indexes).enumerate().narrow('n', *new_indexes)  # XXX
#        else:
#            slc = self.narrow(*indexes)

        return Plot(**named_yindexes)

    def table(self, cons=None):
        """ Returns `presentation.table` from data.

        :param cons: a presentation.table subclass used for constructing.
        :returns: a presentation.table with the values from this table.
        """

        if cons is None:
            from plyny.presentation.text import TextTable
            cons = TextTable

        if self.column_names is None:
            t = cons()
        else:
            t = cons(*self.column_names)

        [t.add(*x) for x in self.as_lists()]

        for i, v in enumerate(self.types()):
            if v == int:
                t.set_justification(i, cons.RIGHT)
            if v == float:
                t.set_justification(i, cons.RIGHT)
                t.set_format(i, '%0.3f')
        return t
