from plyny.collections2.slice import convert_args, countedslice, slice_length


import unittest


class TestCase(unittest.TestCase):
    def test_convert_args(self):
        self.assertEquals(convert_args(0), (0, 0, 1))
        self.assertEquals(convert_args(0, 10), (0, 10, 1))
        self.assertEquals(convert_args(0, 10, 2), (0, 10, 2))
        self.assertEquals(convert_args(2, 10, 2), (2, 10, 2))
        self.assertEquals(convert_args(2, None, 2, 10), (2, 10, 2))
        self.assertEquals(convert_args(2, None, length=10), (2, 10, 1))
        self.assertEquals(convert_args(2), (0, 2, 1))

    def test_countedslice(self):
        s = xrange(10)
        c = countedslice(s, 0, 2)
        self.assertEquals(len(c), 2)
        self.assertEquals(list(c), range(2))
        self.assertRaises(ValueError, list, c)
        c = countedslice(s, 1, 5)
        self.assertEquals(len(c), 4)
        self.assertEquals(list(c), range(1, 5))
        self.assertRaises(ValueError, list, c)

        c = countedslice(s, 1, None)
        self.assertRaises(TypeError, len, c)
        self.assertEquals(list(c), range(1, 10))

    def test_slice_length(self):
        self.assertEquals(slice_length(0, 1), 1)
        self.assertEquals(slice_length(0, 2), 2)
        self.assertEquals(slice_length(1, 2), 1)
        self.assertEquals(slice_length(3), 3)
        self.assertEquals(slice_length(3, None), None)

        self.assertEquals(slice_length(0, 3, 2), 2)
        self.assertEquals(slice_length(0, 4, 2), 2)
        self.assertEquals(slice_length(0, 5, 2), 3)
        self.assertEquals(slice_length(1, 5, 2), 2)
        self.assertEquals(slice_length(1, 31, 6), 5)

if __name__ == '__main__':
    unittest.main()
