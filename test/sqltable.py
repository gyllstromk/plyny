from plyny.sqltable import *

import unittest


class TestCase(unittest.TestCase):
    def test_s(self):
        self.assertRaises(ValueError, SqlDataTable.sqlite3_memory('x'))
        s = SqlDataTable.sqlite3_memory('x')('a', 'b')
        self.assertEquals(s.get('a', 0), None)
        s.set_keys('a')
        s.add(1, 2)
        self.assertEquals(list(s), [(1, 2)])
        self.assertEquals(s.get('a', 1), (1, 2))
        self.assertRaises(ValueError, s.add, (1, 2))
        s.delete('a', 1)
        self.assertEquals(list(s), [])
        self.assertEquals(s.get('a', 0), None)
        s.add(1, 3)
        self.assertEquals(s.get('a', 1), (1, 3))

        s.extend((x, x + 1) for x in xrange(2, 200))
        self.assertTrue(s.get('a', 2), (2, 3))
        self.assertTrue(s.get('a', 199), (199, 200))

        s = SqlDataTable.sqlite3_memory('x')('a', 'b')
        for item in s:
            print item

        from plyny.plio.files import TemporaryDirectory
        t = TemporaryDirectory()
        f = t / 'x.sql'
        s = SqlDataTable.sqlite3(f, 'y')('a', 'b')
        s.add(1, 2)
        self.assertEquals(list(s), [(1, 2)])
        self.assertEquals(s.get('a', 1), (1, 2))
        s = SqlDataTable.sqlite3(f, 'y')('a', 'b')
        self.assertEquals(list(s), [(1, 2)])
        self.assertEquals(s.get('a', 1), (1, 2))


if __name__ == '__main__':
    unittest.main()
