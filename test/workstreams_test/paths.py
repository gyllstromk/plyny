from plyny.plio.files import Path, TemporaryDirectory
from plyny.workstreams.paths import *

import unittest


class TestCase(unittest.TestCase):
    def test_names(self):
        t = IterationWorkstreamPath(None,
                Path('a:b/a/b/c/2011.01.01.01.01.01/output'), None)

        self.assertEquals(t.substream_names(), ['a', 'b', 'c'])
        self.assertEquals(t.process(), 'a:b')

    def test_path(self):
        t = TemporaryDirectory()
        a = t.subdirectory('x').subdirectory('2011.01.01.01.01.01').create()
        b = t.subdirectory('x').subdirectory('2011.01.01.01.01.02').create()
        t.subdirectory('x').subdirectory('current').create()
        File(a.path() / 'a').write('sup')
        File(a.path() / 'b').write('sup')
        File(b.path() / 'a').write('sup')
#        print list(t.subdirectory('x').list_names())
        w = WorkstreamPath.create_with_path(t, 'x', None)
#        self.assertTrue(w.is_workstream())

        e = ExpandedWorkstreamPath.create_with_path(t, 'x', None)
        self.assertEquals(len(e.workstreams()), 1)
        a = e.current()[0]
        self.assertEquals(a.iteration(), '2011.01.01.01.01.02')

        p = e.workstreams()[0].previous()
        self.assertEquals(len(p), 1)

        self.assertEquals(len(w.branches()), 1)

        a = t.subdirectory('y').subdirectory('2011.02.01.01.01.01').create()
        b = t.subdirectory('y').subdirectory('2011.02.01.01.01.02').create()

        w = WorkstreamPath.create_with_path(t, 'x,y', None)
        self.assertEquals(len(w.branches()), 2)

        t.subdirectory('z').subdirectory('y').subdirectory('2011.02.01.01.01.01').create()
        t.subdirectory('z').subdirectory('z').subdirectory('2011.02.01.01.01.01').create()

        e = ExpandedWorkstreamPath.create_with_path(t, 'z')
        self.assertEquals(len(e.workstreams()), 2)

        w = WorkstreamPath.create_with_path(t, 'x')
        self.assertEquals(len(w.workstreams()), 1)
        w = WorkstreamPath.create_with_path(t, 'z')
        self.assertEquals(len(w.workstreams()), 2)

        self.assertEquals(len(w.branches()[0].substreams()), 1)

        w = WorkstreamPath.create_with_path(t, 'x')
        self.assertEquals(len(w.branches()[0].substreams()), 0)

        w = WorkstreamPath.create_with_path(t, '.')

        w = WorkstreamPath.create_with_path(t, '*/y', None)
        l = w.branches()
        self.assertEquals(str(l[0]._path), 'x/y')
        self.assertEquals(str(l[1]._path), 'y/y')
        self.assertEquals(str(l[2]._path), 'z/y')
#        self.assertEquals(l, ['x/y', 'y/y', 'z/y'])

if __name__ == '__main__':
    unittest.main()
