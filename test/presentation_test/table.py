from plyny.presentation import *

import unittest


def make(table):
    t = table('a', 'a & b', 'c # d', '% eee ', 'emmm')
    t.set_justification(0, t.LEFT)
    t.set_justification(2, t.RIGHT)
    t.set_format(1, '%0.3f')
    t.set_format(3, '%0.3f')
    t.set_format(4, '%0.3f')
    t.add(1, 2.5, 0, 0.00000001234, 0.00000000)
    return t


class TestCase(unittest.TestCase):
    def test_hline(self):
        t = TextTable('a')
        t.add(4)
        print(t.render())

    def test_table(self):
        t = make(LatexTable)
        a = t.render()
        b = r"""\begin{tabular}{|l|c|r|c|c|}\hline
a & a \& b & c \# d & \% eee  & emmm \\
\hline
1 & 2.500 & 0 & \ensuremath{1.234 \times 10^{-08}} & \ensuremath{\ll 0.001} \\
\hline
\end{tabular}
"""

        self.assertEquals(a, b)
        t = make(TextTable)
        self.assertEquals(t.render(), t.render())  # had problem of render changing justification values
        self.assertEquals(t.render(), """+------------------------------------------+
| a | a & b | c # d |   % eee   |   emmm   |
+------------------------------------------+
| 1 | 2.500 |     0 | 1.234e-08 | << 0.001 |
+------------------------------------------+""")

        t = make(HtmlTable)
        self.assertEquals(t.render(), """<thead>
<tr>
<th>a</th> <th>a & b</th> <th>c # d</th> <th>% eee </th> <th>emmm</th>
</tr>
</thead>

<tbody>
<tr>
<td>1</td> <td>2.5</td> <td>0</td> <td>1.234e-08</td> <td>0.0</td>
</tr>
</tbody>
""")

        t = LatexTable('a', 'b')
        t.add(1, 2)
        t.insert(2, 3, 2)
        t.multirow = True
        b = r"""\begin{tabular}{|c|c|}\hline
a & b \\
\hline
1 & \multirow{2}{*}{2} \\
3 &   \\
\hline
\end{tabular}
"""
        self.assertEquals(t.render(), b)

        t = LatexTable('a', 'b')
        t.set_format(0, '%0.3f')
        t.add(0.0000000000000, 1)
        self.assertEquals(t.render(), r"""\begin{tabular}{|c|c|}\hline
a & b \\
\hline
\ensuremath{\ll 0.001} & 1 \\
\hline
\end{tabular}
""")

        t = LatexTable('a', 'b')
        t.set_format(0, '%0.6f')
        t.set_format(1, '%0.6f')
        t.add(0.0000000000000, 0.00000001)
        t.set_exponent_cap(1, 3)
        self.assertEquals(t.render(), r"""\begin{tabular}{|c|c|}\hline
a & b \\
\hline
\ensuremath{\ll 0.000001} & \ensuremath{\ll 0.000001} \\
\hline
\end{tabular}
""")

        t = TextTable('a')
        t.set_format(0, '%0.3f')
        t.add('N/A')
        t.add(TextTable.HLINE)
        t.add(3.9221)
        self.assertEquals(t.render(), """+-------+
|   a   |
+-------+
|  N/A  |
+-------+
| 3.922 |
+-------+""")

        t = LatexTable('a', 'b', 'c')
        t.add(1, 2, 3)
        t.add_pair(0, 'ok')
        self.assertEquals(t.render(), r"""\begin{tabular}{|c|c|c|}\hline
\multicolumn{2}{|c|}{ok} & \multirow{2}{*}{c} \\
a & b &  \\
\hline
1 & 2 & 3 \\
\hline
\end{tabular}
""")

        t = TextTable('a')
        t.set_format(0, t.ABBREVIATE)
        for i in xrange(11):
            t.add(19 * 10 ** i)
        self.assertEquals(t.render(), r"""+------+
|  a   |
+------+
|  19  |
| 190  |
|  2K  |
| 19K  |
| 190K |
|  2M  |
| 19M  |
| 190M |
|  2G  |
| 19G  |
| 190G |
+------+""")


if __name__ == '__main__':
    unittest.main()
