Code
====


.. automodule:: plyny.table

	.. autoclass:: plyny.table.DataTable
    	.. automethod:: DataTable.shuffle
    	.. automethod:: DataTable.empty_copy
    	.. automethod:: DataTable.copy
    	.. automethod:: DataTable.extend
    	.. automethod:: DataTable.tape
    	.. automethod:: DataTable.group
    	.. automethod:: DataTable.sum
    	.. automethod:: DataTable.split_into
    	.. automethod:: DataTable.columns
    	.. automethod:: DataTable.clean
    	.. automethod:: DataTable.add
