Workstreams
===========

Workstreams are a way to manage the outputs and inputs of processes. It uses
DataTable as the basic shared object. A process can have multiple Workstreams
as inputs and outputs. Interaction with it is very similar to with DataTable:
the difference is, the metadata and storage are handled for you!

Workstreams are implemented as files within a special directory.

We begin a file which we call 'wstest.py'.

First, let us manage the directory we will use. We can use a helper class
Directory from the included 'plio' library:

  >>> from plyny.plio.files import Directory
  >>>
  >>> d = Directory('our_test_workstreams')

We create a Manager, which :

  >>> from plyny.workstreams import Manager
  >>>
  >>> m = Manager(d)

Creating a DataTable for this process is simple:

  >>> t = m.DataTable('age', 'name')
  >>> t.add(18, 'Mike')
  >>> t.add(22, 'Sara')

After the program, we can view the contents of our workstream directory:

  our_test_workstreams/
     -> wstest/
	     -> 2011.11.23.19.14.16
		     -> config
			 -> output
		 -> current

wstest represents workstreams for the wstest.py process. Each workstream is
saved to a directory with the date of the process execution. As a convenience,
there is always a soft link to the most recent execution called 'current'.
Within the workstream directory, we see a 'config' file, which contains
metadata, and 'output', which contains the process output.

Let's look at config:

	{"column_names": ["age", "name"]}
	{"args": ["wstest.py"]}
	{"length": 2}

It shows the column names we used, the arguments used to run the process (in
this case, only the application mame) and the number of rows in the data.

Now, let's look at output:
   
  [18, "Mike"]
  [22, "Sara"]

Now let's create file wsuser.py:

  >>> from plyny.plio.files import Directory
  >>> from plyny.workstreams import Manager
  >>>
  >>> m = Manager(Directory('our_test_workstreams'))
  >>>
  >>> d = m.open('wstest')
  >>> print d.column_names
  ('age', 'name')
  >>> for item in d:
  ...    print item
  row(age=18, name='Mike')
  row(age=22, name='Sara')

As you can see, it is easy to interact with wstest's output!

Let's look back at the workstream directory of wstest. We see a new file
alongside output and config called 'usage'. This file keeps track of what
processes have used this workstream. The contents are:

{"process": "ws.py", "date": "2011-11-23 19:46:27.502443", "args": []}

What happens if we run wstest again? We see a new file in the execution directory:
	 -> 2011.11.23.19.14.16
		 -> config
		 -> output
		 -> usage
	 -> 2011.11.23.19.17.12
		 -> config
		 -> output
	 -> current

Now current points to the newest execution. When wsuser.py uses wstest's
workstream, it will by default take the most recent execution.

Managing workstreams
--------------------

What happens when processes want to have multiple workstreams? The answer is by 'substreams'. Substreams are a way for a process to include named substreams within it (rather than a per-process default). Creating substreams is straighforward. Let's define a new file called wsmulti.py:

  >>> persons = m.substream('persons').DataTable('age', 'name')
  >>> cars = m.substream('cars').DataTable('make', 'model')
  >>> 
  >>> persons.add(18, 'Sally')
  >>> cars.add('Ford', 'Model-T')

The contents of our directories:

  our_test_workstreams/
  	-> wstest/
	-> wsmulti/
	   -> cars/
	      -> 2011.11.23.19.53.36
	         -> config
	         -> output
		  -> current
	   -> persons/
	      -> 2011.11.23.19.53.36
	         -> config
	         -> output
		  -> current

Let's adapt wsuser:
	>>> persons = m.open('wstest/persons')
	>>> cars = m.open('wstest/cars')

Metadata
--------

We can associate key-value style data with a workstream via the 'config' member. Say we want to add a boolean value called 'final' and a string indicating the author.

  >>> persons = m.DataTable('age', 'name')
  >>> persons.config().final = False
  >>> persons.config().author = 'Karl'

These manifest in the config file:

  {"final": False}
  {"author": "Karl"}

And can be read:
  >>> persons = m.open('wstest/persons')
  >>> print m.config().final
  False
  >>> print m.config().author
  Karl

Streaming
---------

Processes can generate large amounts of data. It is often the case that we wish only to iterate through a process's output without loading it to memory. This is easily accomplished via the streaming interface.

  >>> d = m.stream.open(
