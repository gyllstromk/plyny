.. plyny documentation master file, created by
   sphinx-quickstart on Wed Oct  5 13:01:10 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

plyny documentation
=================================

.. toctree::
  :maxdepth: 2

  tutorial
  workstreams
  libraries
  code

Getting started
===============
