Using DataTable
===============

:class:`plyny.table.DataTable` is like a ``super`` list. It behaves as a 2D
list that enables easy slicing and dicing. Let's begin with creation::

  >>> from plyny import DataTable
  >>> d = DataTable('name', 'age')

This creates a 2-columned :class:`table.DataTable` with column names 'name',
and 'age'.

Adding data is simple::

  >>> d.append('Mack', 30)
  >>> d.append('Alice', 22)

  >>> # add lists or iterables
  >>> x = [('James', 32), ('Sara', 18)]
  >>> d.extend(x)

The table is now populated with two rows. We can inspect some attributes::

  >>> print d.column_names
  ('name', 'age')
  >>> print d.width()
  2

`width` indicates with row size of the table.

We can act on the table as an iterator; it will let us iterate through the
table by row. This row can be accessed as a tuple::

  >>> for item in d:
  ...   print item
  row(name='Mack', age=30)
  row(name='Alice', age=22)
  row(name='James', age=32)
  row(name='Sara', age=18)

  >>> for item in d:
  ...   print item[0], item[1]
  Mack 30
  Alice 22
  James 32
  Sara 18

The rows can also be accessed via the column names::

  >>> for item in d:
  ...   print item.name, item.age

Note: column names must be valid variable names.

A DataTable can be constructed with no column names. In this case, names will
be assigned alphabetically, with the first column being 'a', the second 'b',
and so on. The width of the table will be determined based on the first entry.

We can add many types of variables to a DataTable, including sets, lists,
dicts, strings, floats, and more.

We can also output as a text table::

  >>> print d.table()

Which appears like this::

  +-------------+
  |  name | age |
  +-------------+
  |  Mack |  30 |
  | Alice |  22 |
  +-------------+

ref:table returns an `OutputTable` object.

Splitting and combining
-----------------------

We can split the table into columns::

  >>> names = d.columns('name')
  >>> print names.table()
  +-------+
  |  name |
  +-------+
  |  Mack |
  | Alice |
  +-------+

  >>> names, ages = d.columns('name', 'age')

With no parameters, `columns` returns all columns::

  >>> names, ages = d.columns()

Columns can be combined::

  >>> agenames = ages | names
  >>> print agenames.table()
  +-------------+
  | age |  name |
  +-------------+
  |  30 |  Mack |
  |  22 | Alice |
  +-------------+

Tables can be added::

  >>> d2 = d.empty_copy()
  >>> d2.add('Paul', 60)

  >>> d3 = d + d2
  >>> # equivalent to d3 = d.empty_copy(); d3.extend(d); d3.extend(d2)

  >>> print d3.table()
  +-------------+
  |  name | age |
  +-------------+
  |  Mack |  30 |
  | Alice |  22 |
  |  Paul |  60 |
  +-------------+

Streaming table
---------------

StreamingTable is a variant of DataTable in which data is not stored in memory.
After iterating through a StreamingTable, the data is gone. Conceptually,
StreamingTable is analogous to generator where a DataTable is analogous to a
list.

Interaction with StreamingTable is virtualy identical to DataTable, with the
exception that methods which require keeping data in memory (such as sorting)
will raise Exceptions.

Database-like features
----------------------

DataTable has some db-like features such as joining.

Statistics
----------

Some simple statistics::

  >>> print d.min('age'), d.max('age')
  22, 30

.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`




