from plyny.presentation import text
from plyny.presentation import latex
from plyny.presentation import html

TextTable = text.TextTable
LatexTable = latex.LatexTable
HtmlTable = html.HtmlTable
