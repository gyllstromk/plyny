import sys


try:
    import simplejson as json
#except ImportError:
#    import cjson as json
#    loader = json.decode
#    dumper = json.encode
    if not getattr(json, '_speedups', None):
        print >> sys.stderr, 'WARNING: simplejson c extensions not installed'
except ImportError:
    print >> sys.stderr, 'WARNING: simplejson module not installed; relying upon the *much* slower python json module.'

    import json

loader = json.loads
dumper = json.dumps


def loads(s):
    return loader(s)


def dumps(s):
    return dumper(s)


if __name__ == '__main__':
    print loads(dumps([1, 2, 3]))
