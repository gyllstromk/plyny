def get_next(n):
    return labels(n + 1)[n]


# don't use labels that are reserved words
reserved = set(['as'])


def labels(n):
    from itertools import product
    from string import ascii_letters

    def inner(sz):
        if sz == 1:
            return list(ascii_letters)
        return [y for y in (''.join(x) for x in product(ascii_letters, repeat=sz)) if y not in reserved]

    all_labels = []

    i = 1
    while len(all_labels) < n:
        all_labels += inner(i)
        i += 1

    return all_labels[:n]


if __name__ == '__main__':
    i = 52
    print labels(i)
    print get_next(i)
    i = 53
    print get_next(0)
#    print labels(i)
#    i = 205700
#    print labels(i)
